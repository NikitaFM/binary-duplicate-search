const express = require('express');
const findDuplicate = require('./duplicateFinder');
const app = express();

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/form.html');
});


app.get('/find-duplicate', function (req, res) {
    const input = req.query.dataset;
    if (!input) {
        return res.send('No input specified');
    }

    const dataSet = input.split(',').map((n) => parseInt(n));

    let duplicate;
    let prev;

    for (let n of dataSet) {
        if (
            !n 
            || n < 0 
            || (!prev && n !== 1)
            || (prev && duplicate && n - prev !== 1)
            || (prev && !duplicate && (n - prev !== 1 && n - prev !== 0))
        ) {
            return res.send('Invalid input');
        }

        if (prev === n) {
            duplicate = n;
        }
        prev = n;
    }

    try {
        res.json(findDuplicate(dataSet));
    } catch (e) {
        res.send('Error: ' + e.message);
    }
});

app.listen(process.env.PORT || 3000, function () {
    console.log('Example app listening on port 3000!');
});