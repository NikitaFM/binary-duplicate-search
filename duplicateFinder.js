const { performance } = require('perf_hooks');

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateDataSet(n) {
    const dataSet = [];
    const final = Math.pow(2, n - 1);
    const duplicate = getRandomInt(1, final);

    for (let i = 1; i <= final; i++) {
        dataSet.push(i);

        if (i === duplicate) {
            dataSet.push(i);
        }
    }

    return { dataSet, duplicate };
}

function getSubsetWithDuplicate(dataSet) {
    const pos = Math.ceil(dataSet.length / 2);
    const subset1 = dataSet.slice(0, pos);

    if (subset1[0] + subset1.length - 1 > subset1[subset1.length - 1]) {
        return subset1;
    } else {
        const subset2 =  dataSet.slice(pos);
        if (subset2[0] === subset1[subset1.length - 1]) {
            return [subset2[0]];
        }
        return subset2;
    }
}

function getDuplicate(dataSet) {
    if (dataSet.length === dataSet[dataSet.length - 1]) {
        throw new Error('Data set doesn\'t contain a duplicate.');
    }
    
    let subset = dataSet;
    let steps = 0;

    while (subset.length > 1) {
        steps++;
        subset = getSubsetWithDuplicate(subset);
    }

    return { duplicate: subset[0], steps };
}

module.exports = getDuplicate;

// const n = parseInt(process.argv[2]);
// const { dataSet, duplicate } = generateDataSet(n);
// const timeStarted = performance.now();
// const { duplicate: result, steps } = getDuplicate(dataSet);
// const timeFinished = performance.now();

// console.log(`N = ${n};`);
// console.log(`Data set: ${dataSet.join(', ')};`);
// console.log(`Defined duplicate: ${duplicate};`);
// console.log(`Found duplicate: ${result};`);
// console.log(`Finished in ${timeFinished - timeStarted} ms with ${steps} steps;`);
